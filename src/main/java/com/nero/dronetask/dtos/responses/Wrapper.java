package com.nero.dronetask.dtos.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

@Getter
public class Wrapper<T> {
	private final String message;
	private final T data;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Meta meta;

	public Wrapper(String message) {
		this(message, null, null);
	}

	public Wrapper(T data) {
		this(data, null);
	}

	public Wrapper(String message, T data) {
		this(message, data, null);
	}


	public Wrapper(T data, Meta meta) {
		this("Operation Successful!", data, meta);
	}

	public Wrapper(String message, T data, Meta meta) {
		this.message = message;
		this.data = data;
		this.meta = meta;
	}

	@Getter
	public static class Meta {
		private final long currentPage;
		private final long from;
		private final long to;
		private final long perPage;
		private final long total;
		private final long lastPage;

		public Meta(long currentPage, long from, long to, long perPage, long total, long lastPage) {
			this.currentPage = currentPage;
			this.from = from;
			this.to = to;
			this.perPage = perPage;
			this.total = total;
			this.lastPage = lastPage;
		}
	}
}
