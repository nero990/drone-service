package com.nero.dronetask.dtos.requests;


import lombok.Getter;

import javax.validation.constraints.NotEmpty;

@Getter
public class LoadDroneRequest {

    @NotEmpty
    private Long[] medicationIds;
}
