package com.nero.dronetask.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "medications")
public class Medication extends BaseModel {
    private String name;
    private double weight;
    private String code;
    private String image;
}
